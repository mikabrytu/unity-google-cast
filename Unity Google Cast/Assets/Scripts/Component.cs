﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mikabrytu.GoogleCastPlugin;

public class Component : MonoBehaviour
{
    [SerializeField]
    private GameObject devicePrefab, parent;

    [SerializeField]
    private GameObject devicePanel, mediaPanel;

    private GoogleCastController googleCast;

    private void Start()
    {
        googleCast = new GoogleCastController();
        googleCast.SetApplicationId("7C495457");
    }

    public void ListDevices()
    {
        foreach (Transform child in parent.transform)
            Destroy(child.gameObject);

        List<DeviceModel> devices = googleCast.GetDevices();

        List<MovieModel> movies = new List<MovieModel>();
        movies.Add(new MovieModel
        {
            url = "https://content-prod.funkidsapp.com/episodes/source/ad648159eaf1b7964fe04056ae6570bb161fc8b4_1570158864_dGdHl4uA5pCcoeyMT9zE.mp4",
            duration = 170,
            name = "Desenho 1"
        });
        movies.Add(new MovieModel
        {
            url = "https://content-prod.funkidsapp.com/episodes/source/8c9233edfbe0849b4a6b3572a30196040325f64c_1565034958_loRgftF4Lm9pmI5Pwr21.mp4",
            duration = 124,
            name = "Desenho 2"
        });

        foreach (DeviceModel item in devices)
        {
            var clone = Instantiate(devicePrefab, parent.transform);
            clone.GetComponentInChildren<Text>().text = item.name;
            clone.GetComponent<Button>().onClick.AddListener(delegate {
                googleCast.SetDevice(item);
                googleCast.LoadMedia(movies, () => {
                    googleCast.StartSession(() => ControlMedia());
                });
            });
        }
    }

    public void Play()
    {
        googleCast.PlayMovie();
    }

    public void Pause()
    {
        googleCast.Pause();
    }

    public void Next()
    {
        googleCast.Next();
    }

    public void Previous()
    {
        googleCast.Previous();
    }

    public void Stop()
    {
        googleCast.Stop(() => {
            googleCast.StopSession(() => ControlDevice());
        });
    }

    private void ControlDevice()
    {
        devicePanel.SetActive(true);
        mediaPanel.SetActive(false);
    }

    private void ControlMedia()
    {
        devicePanel.SetActive(false);
        mediaPanel.SetActive(true);
    }
}
