﻿using System.Collections.Generic;
using UnityEngine;
using Mikabrytu.GoogleCastPlugin.Android;

namespace Mikabrytu.GoogleCastPlugin
{
    public class GoogleCastController
    {
        public const string TAG = "#CAST PLUGIN# ";

        public DeviceModel CurrentDevice { get; private set; }
        public string ApplicationId { get; private set; }
        public bool IsCasting { get; private set; }

        private IReceiver receiver;
        private IDevice device;
        private ISession session;
        private IMedia media;

        public GoogleCastController()
        {
            // TODO: Initialize interface automatically based on OS
            receiver = new AndroidReceiverPresenter();
            device = new AndroidDevicePresenter();
            session = new AndroidSessionPresenter();
            media = new AndroidMediaPresenter();
        }


        #region Receiver Functions

        public void SetApplicationId(string id)
        {
            ApplicationId = id;
            receiver.SetApplicationId(ApplicationId);
        }

        #endregion


        #region Device Functions

        public List<DeviceModel> GetDevices()
        {
            if (IsCasting)
                return new List<DeviceModel>();

            List<DeviceModel> deviceList = new List<DeviceModel>();
            var json = SimpleJSON.JSON.Parse(device.GetDevices());

            for (int i = 0; i < json.Count; i++)
            {
                deviceList.Add(new DeviceModel
                {
                    id = json[i]["id"],
                    name = json[i]["name"]
                });
            }

            return deviceList;
        }

        public void SetDevice(DeviceModel model)
        {
            CurrentDevice = model;
        }

        #endregion


        #region Session Functions

        public void StartSession(System.Action OnSuccess)
        {
            session.StartSession(JsonUtility.ToJson(CurrentDevice), () => {
                IsCasting = true;
                OnSuccess.Invoke();
            });
        }

        public void StopSession(System.Action OnSuccess)
        {
            session.StopSession(() => {
                IsCasting = false;
                OnSuccess.Invoke();
            });
        }

        #endregion


        #region Media Functions

        public void LoadMedia(List<MovieModel> movies, System.Action OnSuccess)
        {
            MovieModel[] movieArray = movies.ToArray();
            var json = SimpleJSON.JSON.Parse(JsonHelper.ToJson(movieArray));
            Debug.Log("#CAST PLUGIN# json: " + json["Items"].ToString());
            media.Load(json["Items"].ToString(), () => OnSuccess.Invoke());
        }

        public void PlayMovie()
        {
            media.Play();
        }

        public void Pause()
        {
            media.Pause();
        }

        public void Next()
        {
            media.Next();
        }

        public void Previous()
        {
            media.Previous();
        }

        public void Stop(System.Action OnSuccess)
        {
            media.Stop(() => OnSuccess.Invoke());
        }

        #endregion
    }
}
