﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mikabrytu.GoogleCastPlugin
{
    public interface IDevice
    {
        string GetDevices();
    }
}
