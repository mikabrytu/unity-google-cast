﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mikabrytu.GoogleCastPlugin
{
    public interface IReceiver
    {
        void SetApplicationId(string id);
    }
}
