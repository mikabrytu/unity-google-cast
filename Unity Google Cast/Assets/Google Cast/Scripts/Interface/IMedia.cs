﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mikabrytu.GoogleCastPlugin
{
    public interface IMedia
    {
        void Load(string playlist, System.Action OnSuccess);
        void Play();
        void Pause();
        void Next();
        void Previous();
        void Stop(System.Action OnSuccess);
    }
}
