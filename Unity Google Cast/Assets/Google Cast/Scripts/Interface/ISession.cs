﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mikabrytu.GoogleCastPlugin
{
    public interface ISession
    {
        void StartSession(string device, System.Action OnSuccess);
        void StopSession(System.Action OnSuccess);
    }
}
