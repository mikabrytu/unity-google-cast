﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mikabrytu.GoogleCastPlugin.Android
{
    public class AndroidMediaPresenter : IMedia
    {
        private AndroidJavaObject androidContext, loadBridge, playBridge, pauseBridge, nextBridge, previousBridge, stopBridge;
        private object[] parameters;

        public AndroidMediaPresenter()
        {
            AndroidJavaClass javaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            androidContext = javaClass.GetStatic<AndroidJavaObject>("currentActivity");
            loadBridge = new AndroidJavaObject("com.mikabrytu.nativegooglecast.presenter.movie.LoadMoviePresenter");
            playBridge = new AndroidJavaObject("com.mikabrytu.nativegooglecast.presenter.movie.PlayMoviePresenter");
            pauseBridge = new AndroidJavaObject("com.mikabrytu.nativegooglecast.presenter.movie.PauseMoviePresenter");
            nextBridge = new AndroidJavaObject("com.mikabrytu.nativegooglecast.presenter.movie.NextMoviePresenter");
            previousBridge = new AndroidJavaObject("com.mikabrytu.nativegooglecast.presenter.movie.PreviousMoviePresenter");
            stopBridge = new AndroidJavaObject("com.mikabrytu.nativegooglecast.presenter.movie.StopMoviePresenter");

            parameters = new object[1];
            parameters[0] = androidContext;
        }

        public void Load(string playlist, Action OnSuccess)
        {
            object[] parameters = new object[2];
            parameters[0] = androidContext;
            parameters[1] = playlist;

            var result = loadBridge.Call<bool>("loadMovie", parameters);

            if (result)
                OnSuccess.Invoke();
        }

        public void Play()
        {
            playBridge.Call("playMovie", parameters);
        }

        public void Pause()
        {
            pauseBridge.Call("pauseMovie", parameters);
        }

        public void Next()
        {
            nextBridge.Call("nextMovie", parameters);
        }

        public void Previous()
        {
            previousBridge.Call("previousMovie", parameters);
        }

        public void Stop(Action OnSuccess)
        {
            var result = stopBridge.Call<bool>("stopMovie", parameters);

            if (result)
                OnSuccess.Invoke();
        }
    }
}
