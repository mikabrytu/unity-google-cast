﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mikabrytu.GoogleCastPlugin.Android
{
    public class AndroidReceiverPresenter : IReceiver
    {
        private AndroidJavaObject androidContext, registerBridge;

        public AndroidReceiverPresenter()
        {
            AndroidJavaClass javaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            androidContext = javaClass.GetStatic<AndroidJavaObject>("currentActivity");
            registerBridge = new AndroidJavaObject("com.mikabrytu.nativegooglecast.presenter.receiver.RegisterReceiverPresenter");
        }

        public void SetApplicationId(string id)
        {
            object[] parameters = new object[2];
            parameters[0] = androidContext;
            parameters[1] = id;

            registerBridge.Call("registerReceiver", parameters);
        }
    }
}
