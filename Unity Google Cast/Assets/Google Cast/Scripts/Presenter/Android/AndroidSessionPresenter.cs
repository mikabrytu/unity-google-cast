﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mikabrytu.GoogleCastPlugin.Android
{
    public class AndroidSessionPresenter : ISession
    {
        private AndroidJavaObject androidContext, startBridge, stopBridge;

        public AndroidSessionPresenter()
        {
            AndroidJavaClass javaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            androidContext = javaClass.GetStatic<AndroidJavaObject>("currentActivity");
            startBridge = new AndroidJavaObject("com.mikabrytu.nativegooglecast.presenter.session.StartSessionPresenter");
            stopBridge = new AndroidJavaObject("com.mikabrytu.nativegooglecast.presenter.session.StopSessionPresenter");
        }

        public void StartSession(string device, Action OnSuccess)
        {
            object[] parameters = new object[2];
            parameters[0] = androidContext;
            parameters[1] = device;

            var result = startBridge.Call<bool>("startSession", parameters);

            if (result)
                OnSuccess.Invoke();
        }

        public void StopSession(Action OnSuccess)
        {
            object[] parameters = new object[1];
            parameters[0] = androidContext;

            var result = stopBridge.Call<bool>("stopSession", parameters);

            if (result)
                OnSuccess.Invoke();
        }
    }
}
