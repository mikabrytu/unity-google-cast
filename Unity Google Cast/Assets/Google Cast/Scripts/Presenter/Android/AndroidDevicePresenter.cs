﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mikabrytu.GoogleCastPlugin.Android
{
    public class AndroidDevicePresenter : IDevice
    {
        private AndroidJavaObject androidContext, listBridge;

        public AndroidDevicePresenter()
        {
            AndroidJavaClass javaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            androidContext = javaClass.GetStatic<AndroidJavaObject>("currentActivity");
            listBridge = new AndroidJavaObject("com.mikabrytu.nativegooglecast.presenter.device.ListDevicePresenter");
        }

        public string GetDevices()
        {
            object[] parameters = new object[1];
            parameters[0] = androidContext;

            Debug.Log("#CAST PLUGIN# Calling Native Method");

            var result = listBridge.Call<string>("getDevices", parameters);
            Debug.Log("#CAST PLUGIN# devices: " + result);

            return result;
        }
    }
}
