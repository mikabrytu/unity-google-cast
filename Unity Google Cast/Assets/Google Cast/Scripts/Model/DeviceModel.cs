﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mikabrytu.GoogleCastPlugin
{
    [Serializable]
    public struct DeviceModel
    {
        public string id;
        public string name;
    }
}
