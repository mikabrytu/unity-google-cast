﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mikabrytu.GoogleCastPlugin
{
    [Serializable]
    public struct MovieModel
    {
        public string name;
        public string description;
        public int season;
        public int episode;
        public string url;
        public long duration;
    }
}
