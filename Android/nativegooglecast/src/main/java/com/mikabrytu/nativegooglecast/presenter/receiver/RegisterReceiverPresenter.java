package com.mikabrytu.nativegooglecast.presenter.receiver;

import android.content.Context;
import android.os.Handler;

import static com.mikabrytu.nativegooglecast.utils.NativeDependencies.getCastContext;

public class RegisterReceiverPresenter {

    public void registerReceiver(final Context context, final String id) {
        new Handler(context.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                getCastContext(context).setReceiverApplicationId(id);
            }
        });
    }
}
