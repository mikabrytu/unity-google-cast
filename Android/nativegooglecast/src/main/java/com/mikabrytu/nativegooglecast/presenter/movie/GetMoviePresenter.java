package com.mikabrytu.nativegooglecast.presenter.movie;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import org.json.JSONObject;

import static com.mikabrytu.nativegooglecast.utils.NativeDependencies.TAG;
import static com.mikabrytu.nativegooglecast.utils.NativeDependencies.getCastContext;

public class GetMoviePresenter {

    private String movieResult;
    private boolean movieReady = false;

    public String getMovie(final Context context) {
        new Handler(context.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                try {
                    RemoteMediaClient mediaClient = getCastContext(context)
                            .getSessionManager()
                            .getCurrentCastSession()
                            .getRemoteMediaClient();

                    if (mediaClient == null) {
                        movieResult = "empty";
                        movieReady = true;
                    } else {
                        JSONObject json = mediaClient
                                .getMediaInfo()
                                .getCustomData();

                        movieResult = json.toString();
                        movieReady = true;
                    }
                } catch (Exception e) {
                    Log.e(TAG, e.toString());

                    movieResult = "";
                    movieReady = true;
                }
            }
        });

        while (!movieReady) {}

        return movieResult;
    }
}
