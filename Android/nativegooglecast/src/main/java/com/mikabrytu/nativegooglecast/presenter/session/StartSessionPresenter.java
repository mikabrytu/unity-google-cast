package com.mikabrytu.nativegooglecast.presenter.session;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import androidx.mediarouter.media.MediaRouter;
import com.google.android.gms.cast.CastStatusCodes;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaLoadRequestData;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.gson.Gson;
import com.mikabrytu.nativegooglecast.R;
import com.mikabrytu.nativegooglecast.manager.ScanManager;
import com.mikabrytu.nativegooglecast.listener.SessionListener;
import com.mikabrytu.nativegooglecast.model.Device;
import com.mikabrytu.nativegooglecast.model.Movie;

import java.util.concurrent.Callable;

import static com.mikabrytu.nativegooglecast.utils.NativeDependencies.*;

public class StartSessionPresenter {

    private ScanManager scanManager;
    private boolean sessionReady = false;

    public boolean startSession(final Context context, String deviceData) {
        if (scanManager == null)
            scanManager = new ScanManager(context);

        Gson gson = new Gson();

        final Device device = gson.fromJson(deviceData, Device.class);

        scanManager.startScan(new Callable<Void>() {
            @Override
            public Void call() {
                return execute(context, device);
            }
        }, new Callable<String>() {
            @Override
            public String call() {
                Log.e(TAG, "Exception on calling startSession()");

                sessionReady = true;

                return null;
            }
        });

        while (!sessionReady) {}

        return true;
    }

    private Void execute(final Context context, final Device device) {
        scanManager.stopScan();

        new Handler(context.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                for (MediaRouter.RouteInfo item : getMediaRouter(context).getRoutes()) {
                    if (item.getId().contains(device.getId())) {
                        Log.d(TAG, "Route "+ device.getName() +" selected");
                        getMediaRouter(context).selectRoute(item);
                        sessionReady = true;
                    }
                }
            }
        });

        return null;
    }
}
