package com.mikabrytu.nativegooglecast.utils;

import android.content.Context;
import androidx.mediarouter.media.MediaRouter;
import com.google.android.gms.cast.framework.CastContext;

public class NativeDependencies {

    public static String TAG = "CastPlugin";

    public static CastContext getCastContext(Context context) {
        return CastContext.getSharedInstance(context);
    }

    public static MediaRouter getMediaRouter(Context context) {
        return MediaRouter.getInstance(context);
    }

}
