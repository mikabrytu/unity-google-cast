package com.mikabrytu.nativegooglecast.manager;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import androidx.mediarouter.media.MediaRouteSelector;
import androidx.mediarouter.media.MediaRouter;
import com.google.android.gms.cast.CastMediaControlIntent;
import com.mikabrytu.nativegooglecast.R;

import java.util.concurrent.Callable;

import static com.mikabrytu.nativegooglecast.utils.NativeDependencies.TAG;
import static com.mikabrytu.nativegooglecast.utils.NativeDependencies.getMediaRouter;

public class ScanManager {

    private Context context;
    private Callable callable, error;

    final MediaRouter.Callback scanCallback = new MediaRouter.Callback() {
        @Override
        public void onRouteChanged(MediaRouter router, MediaRouter.RouteInfo route) {
            responseHandler(true);
        }
    };

    public ScanManager(Context context) {
        this.context = context;
    }

    public void startScan(final Callable callable, final Callable<String> error) {
        this.callable = callable;
        this.error = error;

        Runnable scanTask = new Runnable() {
            @Override
            public void run() {
                try {
                    MediaRouter mediaRouter = getMediaRouter(context);
                    MediaRouteSelector selector = new MediaRouteSelector.Builder()
                            .addControlCategory(CastMediaControlIntent.categoryForCast(context.getString(R.string.app_id)))
                            .build();
                    mediaRouter.addCallback(selector, scanCallback, MediaRouter.CALLBACK_FLAG_PERFORM_ACTIVE_SCAN);
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                    responseHandler(false);
                }
            }
        };
        new Handler(context.getMainLooper()).post(scanTask);
    }

    public void stopScan() {
        Runnable stopTask = new Runnable() {
            @Override
            public void run() {
                getMediaRouter(context).removeCallback(scanCallback);
            }
        };
        new Handler(context.getMainLooper()).post(stopTask);
    }

    private void responseHandler(boolean success) {
        if (success) {
            try {
                callable.call();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                error.call();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
