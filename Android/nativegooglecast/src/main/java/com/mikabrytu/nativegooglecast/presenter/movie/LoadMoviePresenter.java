package com.mikabrytu.nativegooglecast.presenter.movie;

import android.content.Context;
import android.os.Handler;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import com.google.android.gms.cast.CastStatusCodes;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.MediaQueueItem;
import com.google.android.gms.cast.framework.CastSession;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mikabrytu.nativegooglecast.R;
import com.mikabrytu.nativegooglecast.listener.SessionListener;
import com.mikabrytu.nativegooglecast.model.Movie;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static com.mikabrytu.nativegooglecast.utils.NativeDependencies.TAG;
import static com.mikabrytu.nativegooglecast.utils.NativeDependencies.getCastContext;

public class LoadMoviePresenter {

    private Gson gson;
    private boolean loadReady = false;

    public boolean loadMovie(final Context context, String movieData) {
        gson = new Gson();

        final List<Movie> movieList = gson.fromJson(movieData, new TypeToken<List<Movie>>(){}.getType());

        new Handler(context.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                try {
                    CastSession castSession = getCastContext(context)
                            .getSessionManager()
                            .getCurrentCastSession();

                    if (castSession != null) {
                        loadMovie(context, castSession, movieList);
                    } else {
                        SessionListener sessionListener = new SessionListener() {
                            @Override
                            public void onSessionStarted(CastSession session, String s) {
                                loadMovie(context, session, movieList);
                            }

                            @Override
                            public void onSessionStartFailed(CastSession session, int i) {
                                Log.e(TAG, "Session Failed because of "+ CastStatusCodes.getStatusCodeString(i));
                                loadReady = true;
                            }
                        };

                        getCastContext(context).getSessionManager()
                                .addSessionManagerListener(sessionListener, CastSession.class);
                    }

                    loadReady = true;
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                    loadReady = true;
                }
            }
        });

        while (!loadReady) {}

        return true;
    }

    private void loadMovie(Context context, CastSession session, List<Movie> movieList) {
        MediaQueueItem[] queue = new MediaQueueItem[movieList.size()];
        for (int i = 0; i < movieList.size(); i++) {
            Movie movie = movieList.get(i);
            JSONObject customData = new JSONObject();

            if (gson == null)
                gson = new Gson();

            try {
                String json = gson.toJson(movie, Movie.class);
                customData = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }


            MediaMetadata metadata = new MediaMetadata(MediaMetadata.MEDIA_TYPE_MOVIE);
            metadata.putString(MediaMetadata.KEY_TITLE, movie.getName());
            // TODO: Set other movie data

            MediaInfo mediaInfo = new MediaInfo.Builder(movie.getUrl())
                    .setStreamType(MediaInfo.STREAM_TYPE_BUFFERED)
                    .setContentType(context.getString(R.string.movie_content_type))
                    .setMetadata(metadata)
                    .setStreamDuration(movie.getDuration())
                    .setCustomData(customData)
                    .build();

            queue[i] = new MediaQueueItem.Builder(mediaInfo)
                    .setAutoplay(true)
                    .setStartTime(0)
                    .build();
        }

        session.getRemoteMediaClient()
                .queueLoad(queue, 0, PlaybackStateCompat.REPEAT_MODE_GROUP, null);
    }
}
