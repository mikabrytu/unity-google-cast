package com.mikabrytu.nativegooglecast.presenter.session;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import androidx.mediarouter.media.MediaRouter;

import static com.mikabrytu.nativegooglecast.utils.NativeDependencies.TAG;
import static com.mikabrytu.nativegooglecast.utils.NativeDependencies.getMediaRouter;

public class StopSessionPresenter {

    public boolean stopSession(final Context context) {
        new Handler(context.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                try {
                    getMediaRouter(context).unselect(MediaRouter.UNSELECT_REASON_DISCONNECTED);
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
            }
        });

        return true;
    }

}
