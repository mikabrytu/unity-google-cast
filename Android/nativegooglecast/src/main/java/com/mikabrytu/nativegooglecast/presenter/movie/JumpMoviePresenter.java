package com.mikabrytu.nativegooglecast.presenter.movie;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;

import static com.mikabrytu.nativegooglecast.utils.NativeDependencies.TAG;
import static com.mikabrytu.nativegooglecast.utils.NativeDependencies.getCastContext;

public class JumpMoviePresenter {

    public void jump(final Context context, final int jump) {
        new Handler(context.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                try  {
                    RemoteMediaClient mediaClient = getCastContext(context)
                            .getSessionManager()
                            .getCurrentCastSession()
                            .getRemoteMediaClient();

                    if (mediaClient != null) {
                        long j = mediaClient.getApproximateStreamPosition() + (jump * 1000);
                        mediaClient.seek(j);
                    }
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }
}
