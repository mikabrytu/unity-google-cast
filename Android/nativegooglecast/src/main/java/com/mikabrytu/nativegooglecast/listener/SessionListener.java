package com.mikabrytu.nativegooglecast.listener;

import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.SessionManagerListener;

public class SessionListener implements SessionManagerListener<CastSession> {
    @Override
    public void onSessionStarting(CastSession session) {

    }

    @Override
    public void onSessionStarted(CastSession session, String s) {

    }

    @Override
    public void onSessionStartFailed(CastSession session, int i) {

    }

    @Override
    public void onSessionEnding(CastSession session) {

    }

    @Override
    public void onSessionEnded(CastSession session, int i) {

    }

    @Override
    public void onSessionResuming(CastSession session, String s) {

    }

    @Override
    public void onSessionResumed(CastSession session, boolean b) {

    }

    @Override
    public void onSessionResumeFailed(CastSession session, int i) {

    }

    @Override
    public void onSessionSuspended(CastSession session, int i) {

    }
}
