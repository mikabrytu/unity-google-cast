package com.mikabrytu.nativegooglecast.presenter.movie;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import static com.mikabrytu.nativegooglecast.utils.NativeDependencies.TAG;
import static com.mikabrytu.nativegooglecast.utils.NativeDependencies.getCastContext;

public class NextMoviePresenter {

    public void nextMovie(final Context context) {
        new Handler(context.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                try {
                    getCastContext(context)
                            .getSessionManager()
                            .getCurrentCastSession()
                            .getRemoteMediaClient()
                            .queueNext(null);
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }
}
