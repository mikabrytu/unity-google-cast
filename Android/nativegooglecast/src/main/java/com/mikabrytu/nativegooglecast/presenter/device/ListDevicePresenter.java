package com.mikabrytu.nativegooglecast.presenter.device;

import android.content.Context;
import android.util.Log;
import androidx.mediarouter.media.MediaRouter;
import com.google.android.gms.cast.CastDevice;
import com.google.gson.Gson;
import com.mikabrytu.nativegooglecast.manager.ScanManager;
import com.mikabrytu.nativegooglecast.model.Device;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import static com.mikabrytu.nativegooglecast.utils.NativeDependencies.TAG;
import static com.mikabrytu.nativegooglecast.utils.NativeDependencies.getMediaRouter;

public class ListDevicePresenter {

    private ScanManager scanManager;
    private String getDevicesResult;
    private boolean deviceReady = false;

    public String getDevices(final Context context) {
        if (scanManager == null)
            scanManager = new ScanManager(context);

        scanManager.startScan(new Callable<Void>() {
            @Override
            public Void call() {
                return execute(context);
            }
        }, new Callable<String>() {
            @Override
            public String call() {
                Log.e(TAG, "Exception on calling getDevices()");

                getDevicesResult = "";
                deviceReady = true;

                return null;
            }
        });

        while (!deviceReady) {}

        return getDevicesResult;
    }

    private Void execute(final Context context) {
        scanManager.stopScan();

        List<Device> list = new ArrayList<>();
        Gson gson = new Gson();

        for (MediaRouter.RouteInfo item : getMediaRouter(context).getRoutes()) {
            CastDevice castDevice = CastDevice.getFromBundle(item.getExtras());

            if (castDevice != null) {
                Device device = new Device();
                device.setId(castDevice.getDeviceId());
                device.setName(castDevice.getFriendlyName());

                list.add(device);
            }
        }

        getDevicesResult = gson.toJson(list);
        deviceReady = true;

        Log.d(TAG, getDevicesResult);
        return null;
    }
}
