package com.mikabrytu.nativegooglecast.presenter.movie;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;

import static com.mikabrytu.nativegooglecast.utils.NativeDependencies.TAG;
import static com.mikabrytu.nativegooglecast.utils.NativeDependencies.getCastContext;

public class CheckMovieStatusPresenter {

    private int checkResult;
    private boolean checkReady = false;

    public int checkMovieStatus(final Context context) {
        new Handler(context.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                try {
                    RemoteMediaClient mediaClient = getCastContext(context)
                            .getSessionManager()
                            .getCurrentCastSession()
                            .getRemoteMediaClient();

                    if (mediaClient == null) {
                        checkResult = 1;
                        checkReady = true;
                        return;
                    }

                    if (mediaClient.isPlaying()) {
                        checkResult = 3;
                        checkReady = true;
                    }

                    if (mediaClient.isPaused()) {
                        checkResult = 4;
                        checkReady = true;
                    }
                } catch (Exception e) {
                    Log.e(TAG, e.toString());

                    checkResult = 0;
                    checkReady = true;
                }
            }
        });

        while (!checkReady) {}

        return checkResult;
    }
}
